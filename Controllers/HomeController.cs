﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using SampleMerchant.Models;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SampleMerchant.Controllers
{
    public class HomeController : Controller
    {
        private const string PUBLIC_KEY = @"-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEApw+/1PUjb9ErwROchXI8
VbHcLrY2YY6KvGhDaJqS+Crl3t1ZMCnYe92CkR8gf7i8XXDh7crjw+7S1BSL97eg
QDOMUonaxTyYq9UqU8tOQkqvEH8FXttIYLb3LhD6ciNwpPWCfzKRTWymgx62CLKM
F7579OzTE3BmihEQKZq8u7pHrGdSM9zY63WsE/wZnoOziwCHvN+9nDhiiwyCTdJ2
309mdX5zFQ6K+TQ5KC1/XHyXWbxAZ6uPpSVBA3mIHirc/Xv3f9ooFVqxGaO7QsHn
RjjVH9CwheIR746ZkzdCDGj5mcqCfWTu32zSMzJ51mf6SM/77gKduwsKe33pbilU
SRskY53Cv9EQLPgl/IorK9ukxueAk6SJgZKBBGGetJ2bbcbKGFktgOguFTbUev+7
anH1XzI9ZKcHgNJhc511QadpYoaqOTUx5ZhMNMvkIfndrcs6p+8V6loAapDej9eL
lnmxTcERSEfAXq1FCcOxwiEnNdkdYD2MAXd9UGdhcdGgChuSKubH43P2C0zLgW9Q
+2s/iiTX8mi1hodzJ+TYH6l12ZyuxgLB0o/6V+bsGG1O3ds8AF2dOVrTDUdC1bhM
dLBpQtRV20uX0XKuCUubHFHbcdqpnZ/pWNsxplOYbjWfsox6oXINysSK6JE+tuS3
rBM5WhalJd/fNCcCmcaD9iECAwEAAQ==
-----END PUBLIC KEY-----";

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CheckoutResult(string ResponseCode, string TransactionID, string Token, string Signature)
        {
            ViewBag.Message = "Checkout Result";
            ViewBag.TransactionID = TransactionID;
            ViewBag.Result = ResponseCode == "00" ? "Success"
                : "Unsuccess";
            var dto = new { ResponseCode, TransactionID, Token };
            ViewBag.VerifySignature = VerifySignature(CreateDataToSign(dto), Signature)
                ? "Signature verified ok" : "Signature Verified failed";
            return View();
        }

        [HttpPost]
        public string Signature(Order order)
        {
            var dataToSign = CreateDataToSign(order);

            return GenerateSignature(dataToSign);
        }

        [HttpPost]
        public async Task<object> SecurePay(SecurePay securePay)
        {
            var url = securePay.URL;
            securePay.URL = null;
            var dataToSign = CreateDataToSign(securePay);
            securePay.Signature = GenerateSignature(dataToSign);
            return Json(await Post<SecurePayResponse>(url, securePay));
        }


        [HttpPost]
        public async Task<object> VerifyToken(VerifyToken verifyToken)
        {
            var url = verifyToken.URL;
            verifyToken.URL = null;
            var dataToSign = CreateDataToSign(verifyToken);
            verifyToken.Signature = GenerateSignature(dataToSign);
            return Json(await Post<VerifyTokenResponse>(url, verifyToken));
        }

        [NonAction]
        public static string CreateDataToSign(object obj)
        {
            var t = obj.GetType();
            var orderType = obj.GetType();
            var properties = orderType
                .GetProperties();

            var sortedArr = properties.OrderBy(x => x.Name).ToArray();

            var sb = new StringBuilder();
            foreach (var prop in sortedArr)
            {
                if (prop.CanRead)
                {
                    if ((prop.PropertyType.Name == "String"
                        || prop.PropertyType.Name == "Decimal") && prop.Name != "Signature")
                    {
                        var value = prop.GetValue(obj);
                        sb.Append(value?.ToString());
                    }
                }

            }
            return sb.ToString();
        }

        [NonAction]
        public static string GenerateSignature(string data)
        {
            X509Certificate2 cert = new X509Certificate2(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Keys\\testMerchant.p12"),
                "1234", X509KeyStorageFlags.Exportable | X509KeyStorageFlags.PersistKeySet);
            RSACryptoServiceProvider privateKey = (RSACryptoServiceProvider)cert.PrivateKey;
            ASCIIEncoding ByteConverter = new ASCIIEncoding();
            byte[] originalData = Encoding.UTF8.GetBytes(data);
            var signedHash = privateKey.SignData(originalData, new MD5CryptoServiceProvider());
            return Convert.ToBase64String(signedHash);
        }

        [NonAction]
        public static bool VerifySignature(string data, string signature, string publicKey = null)
        {
            try
            {
                publicKey = publicKey ?? PUBLIC_KEY;

                AsymmetricKeyParameter keyPair = (AsymmetricKeyParameter)
                    new PemReader(new StringReader(publicKey)).ReadObject();

                var dataBytes = Encoding.UTF8.GetBytes(data);
                var signatureBytes = Convert.FromBase64String(signature);
                ISigner signer = SignerUtilities.GetSigner("MD5withRSA");
                signer.Init(false, keyPair);
                signer.BlockUpdate(dataBytes, 0, dataBytes.Length);
                return signer.VerifySignature(signatureBytes);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [NonAction]
        private async Task<T> Post<T>(string url, object obj)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                string json = new JavaScriptSerializer().Serialize(obj);
                var response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
                var content = await response.Content.ReadAsStringAsync();
                return new JavaScriptSerializer().Deserialize<T>(content);
            }
        }

    }
}