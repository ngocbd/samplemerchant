﻿using System.Collections.Generic;

namespace SampleMerchant.Models
{
    public class Order
    {
        public string ProfileID { get; set; }
        public string AccessKey { get; set; }
        public string TransactionID { get; set; }
        public string TransactionDateTime { get; set; }
        public string Language { get; set; }
        public string Description { get; set; }
        public decimal TotalAmount { get; set; }
        public string Currency { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }

        public string Country { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string ReturnUrl { get; set; }

        public string CancelUrl { get; set; }

        public string Signature { get; set; }
        public List<Item> Items { get; set; } = new List<Item>();

    }

    public class Item
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
    }
}