﻿using System.Collections.Generic;

namespace SampleMerchant.Models
{
    public class SecurePay
    {
        public string ProfileID { get; set; }
        public string AccessKey { get; set; }
        public string TransactionID { get; set; }
        public string TransactionDateTime { get; set; }
        public string Description { get; set; }
        public decimal TotalAmount { get; set; }
        public string Currency { get; set; }
        public string ReturnUrl { get; set; }
        public string Token { get; set; }
        public string Signature { get; set; }
        public string URL { get; set; }
    }

    public class SecurePayResponse : DefaultCheckoutResponse
    {
        public string ACSUrl { get; set; }
        public string PaReq { get; set; }
        public string MD { get; set; }
        public string TermUrl { get; set; }
    }

    public class DefaultCheckoutResponse
    {
        public string TransactionID { get; set; }
        public string Token { get; set; }
        public string ResponseCode { get; set; }
        public string Signature { get; set; }
    }
}