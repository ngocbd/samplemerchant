﻿using System.Collections.Generic;

namespace SampleMerchant.Models
{
    public class VerifyToken
    {
        public string ProfileID { get; set; }
        public string AccessKey { get; set; }
        public string TransactionID { get; set; }
        public string TransactionDateTime { get; set; }
        public string Token { get; set; }
        public string Signature { get; set; }
        public string URL { get; set; }
    }

    public class VerifyTokenResponse : DefaultCheckoutResponse
    {
        public string CardNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Email { get; set; }

    }
}